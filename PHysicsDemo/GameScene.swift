//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    
    override func didMove(to view: SKView) {
        
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        
        circle.position = CGPoint(x:self.size.width*0.25, y:self.size.height/2)
        square.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        triangle.position = CGPoint(x:self.size.width*0.75, y:self.size.height/2)

        
        
        // DRAW HITBOX
        self.circle.physicsBody = SKPhysicsBody(circleOfRadius: self.circle.frame.width/2)
        self.square.physicsBody = SKPhysicsBody(rectangleOf: self.square.frame.size)
        self.triangle.physicsBody = SKPhysicsBody(rectangleOf: self.triangle.frame.size)
        //self.triangle.physicsBody = SKPhysicsBody(texture: self.triangle.texture!, size: self.triangle.size)

        
        
        // GRAVITY OFF
        self.circle.physicsBody?.affectedByGravity = true
        self.square.physicsBody?.affectedByGravity = true
        self.triangle.physicsBody?.affectedByGravity = true
        
        self.square.physicsBody?.angularVelocity = 10
        self.triangle.physicsBody?.angularVelocity = 1
        
        self.circle.physicsBody?.restitution = 1
        self.circle.physicsBody?.density = 36
        self.square.physicsBody?.density = 32
        

        
        addChild(circle)
        addChild(square)
        addChild(triangle)
    
        spawnSand()
        update(2)
    }
    
    
    func spawnSand(){
        let sand = SKSpriteNode(imageNamed: "sand")
        
        let x = self.size.width - 10
        let y = self.size.height - 10
        
        sand.position.x = x;
        sand.position.y = y;
        
        
        sand.physicsBody = SKPhysicsBody(rectangleOf: sand.frame.size)
        sand.physicsBody?.affectedByGravity = true
        sand.physicsBody?.restitution = 1
        sand.physicsBody?.density = 35
        addChild(sand)
    }
    
    override func update(_ currentTime: TimeInterval) {
        self.spawnSand()
    }
}
